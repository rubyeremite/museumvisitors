class Ticket < ActiveRecord::Base
  attr_accessor :reqd_time

  class << self
    def inside_members(reqdtime)
      @reqd_time = reqdtime
      (member_status('entry') -member_status('exit')).count
    end

    def member_status(status)
      Ticket.where(
          '(date(timestamp) >= ? AND timestamp <= ? ) and event_type = ?',
          @reqd_time.to_date,
          @reqd_time,
          status.to_s).pluck(:ticket_id).uniq
    end
  end
end
