json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :ticket_id, :event_type, :timestamp
  json.url ticket_url(ticket, format: :json)
end
