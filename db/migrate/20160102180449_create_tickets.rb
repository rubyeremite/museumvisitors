class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :ticket_id
      t.string :event_type, limit: 6
      t.datetime :timestamp

      t.timestamps null: false
    end
  end
end
