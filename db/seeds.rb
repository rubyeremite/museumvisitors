
def get_time(day, time)
  DateTime.new(2015, 12, day, time, min_sec, min_sec)
end

def min_sec
  (rand(0..59))
end

def create_entry(entry_type, ticket_id, entry_exit_time)
  Ticket.create(event_type: entry_type,
                ticket_id: ticket_id,
                timestamp: entry_exit_time)
end

25_000.times do |n|
  day = (rand(1..31)).to_i
  entry_hour = (rand(9..15)).to_i
  exit_hour = 1 + rand(entry_hour..17)
  create_entry('entry', n, get_time(day, entry_hour))
  create_entry('exit', n, get_time(day, exit_hour))
end
